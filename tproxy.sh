#! /bin/bash
# Shell script for doing tproxy
# Autor Firdavs Murodov <firdavs@root.tj>
# Date Mar 27, 2015 14:00

# FLUSH RULES
iptables -F
iptables -F -t nat
iptables -F -t mangle
iptables -t mangle -X DIVERT

ip rule del fwmark 1 lookup 100
ip route del local 0.0.0.0/0 dev lo table 100
#
#Incoming interface (from Clients)
IN_IF=vlan3815

# The following hosts/nets will be proxied
TPROXY='62.122.136.0/21 192.168.0.0/16 10.0.0.0/8'

# The following hosts/nets will not be proxied
NOTPROXY='192.168.1.0/24 192.168.4.2 10.10.10.25'


function mk_ipt()
{
 iptables -t mangle -N DIVERT
 iptables -t mangle -A PREROUTING -p tcp -m socket -j DIVERT
 iptables -t mangle -A DIVERT -j MARK --set-mark 1
 iptables -t mangle -A DIVERT -j ACCEPT

 ip rule add fwmark 1 lookup 100
 ip route add local 0.0.0.0/0 dev lo table 100

 for i in $NOTPROXY; do
  iptables -t mangle -A PREROUTING -p tcp -s $i --dport 80 -j ACCEPT
  iptables -t mangle -A PREROUTING -p tcp -s $i --dport 80 -j RETURN
 done;

for i in $TPROXY; do
  iptables -t mangle -A PREROUTING -i $IN_IF -p tcp -s $i --dport 80 -j TPROXY --tproxy-mark 0x1/0x1 --on-ip 127.0.0.1 --on-port 3129
 done;
}

mk_ipt
