# tproxy
Troxy cache on squid3  
<http://roottajikistan.github.io/tproxy/>

### configure:  
######  /etc/squid3/squid.conf  
  
`http_port 127.0.0.1:3128 intercept`  
`http_port 127.0.0.1:3129 tproxy` 

  
###### /etc/iproute2/rt_tables  
`100 troxy`  
  
Documentation for tproxy:  
<http://wiki.squid-cache.org/Features/Tproxy4>  
<https://www.kernel.org/doc/Documentation/networking/tproxy.txt>

